/* Author: 

*/
window.onload=init();

function init(){


 /* ============================
    Additional Data for modal 
    =============================*/

  var data =[
    {
      "description":"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis minima illo sit delectus, magni tempore. Reiciendis voluptatum mollitia doloremque libero, alias aut molestias. Dicta necessitatibus dolore, expedita dolorum explicabo. Id."
    },
    {
      "description":"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here."
    },
    {
      "description":"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock."
    },
    {
      "description":"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly ta necessitatibus dolore, expedita dolorum explicabo. Id."
    },
    {
      "description":"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident"
    },
    {
      "description":"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain."
    },
    {
      "description":"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself"
    },
    {
      "description":"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis minima illo sit delectus, magni tempore. Reiciendis voluptatum mollitia doloremque libero, alias aut molestias. Dicta necessitatibus dolore, expedita dolorum explicabo. Id."
    }
  ]

/* ============================
    variable Declaration
    =============================*/

  var pop_up, modal_data, close_btn,modal_ul,modal_li,i,content,index,container;

  pop_up = document.getElementById('popup');
  modal_data = document.querySelector('.modal-container ul');
  modal_contain = document.getElementsByClassName('modal-container')
  close_btn = document.getElementById('close');

  modal_ul = document.getElementById('modal-pg');

  modal_li=modal_ul.children;
  console.log(modal_li);

/* ==============================
    Event-Listner for modal click
    =============================*/

  for(i=0; i<modal_li.length; i++){
    console.log(modal_li[i]);
    modal_li[i].addEventListener('click',function() { pop_modal(this); });
  }


/* ==============================
    function for modal popup
    =============================*/

function pop_modal(list) {
  data_li = list.cloneNode(list);
  index=list.getAttribute('data-modal');
  new_li = document.createElement('li');
  new_li.setAttribute('class','child-li');
  content=document.createTextNode(data[index].description);
  new_li.appendChild(content);
  modal_data.appendChild(data_li)
  modal_data.appendChild(new_li);
  modal_contain[0].appendChild(modal_data);
  pop_up.appendChild(modal_contain[0]);
  console.log(pop_up);
  pop_up.classList.add('open');
  close_btn.style.display="block";
  close_btn.addEventListener('click',function(e) { close_modal(this,pop_up,e) });
  window.addEventListener('click',function(e,mypopup) { close_window(e,pop_up) });
}

/* ==============================
    function for close btn
    =============================*/

  function close_modal(btn,mypopup,event) {
    if(event.target == mypopup || event.target == close_btn){
      ele=document.querySelector('.modal-container ul li');
      li=document.getElementsByClassName('child-li');
      mypopup.classList.remove('open');

      if(ele!== null && li[0]!== null) { 
          modal_data.removeChild(ele);
          modal_data.removeChild(li[0]);
        }
        else { console.log("ele not presend");}
    }  
    console.log(mypopup);    
  }


function close_window(e,mypopup) {
  if(e.target == popup) {
    console.log("click");
     ele=document.querySelector('.modal-container ul li');
      li=document.getElementsByClassName('child-li');
      console.log(mypopup)
      mypopup.classList.remove('open');
      if(ele!== null && li[0]!==null) { 
        // break;
          modal_data.removeChild(ele);
          modal_data.removeChild(li[0]);
        }
    }
  }
}


/* ==========================================================================
   Tab Funtionality
  ==========================================================================*/


/* ============================
    variable Declaration
    =============================*/
var li_tab, li_content, showId,i;

  li_tab = document.querySelector('.tab-child').children;
  li_content = document.querySelector('.tab-content ul').children;

 for(i=0;i<li_tab.length;i++) {
    li_tab[i].addEventListener('click',function(){ day(this); });
  } 


/* ==============================
    function for active class
    =============================*/

  function day (current) {

   for(i=0;i<li_tab.length;i++) {
    if(li_tab[i].classList == 'active'){
      li_tab[i].classList.remove('active');
    }
   }
   current.classList.add('active');
   showId=current.children[0].getAttribute('data-tab');
   
/* ==============================
    function for Tab 
    =============================*/

   for(i=0; i<li_content.length; i++) {
    if(li_content[i].classList.contains('open')) {
      li_content[i].classList.remove('open');
      console.log(li_content);
    }
   document.getElementById(showId).classList.add('open');
   }
}

/* ===========================================
Hamburger Menu 
===========================================*/

var hamburger, menu, menuClose;
  hamburger = document.getElementById('hamburgerMenu');
  menu = document.getElementById('myMenu');
  menuClose = document.getElementById('menuClose');

hamburger.addEventListener('click', function() {
  menu.style.left = "0";
});

menuClose.addEventListener('click', function() {
  menu.style.left = "-250px";
});


/* ===========================================
 Search Click 
===========================================*/
var searchClick, searchClick, searchClose;

  searchClick = document.getElementById('searchClick');
  form=document.querySelector('form label');
  searchClick = document.getElementById('searchParent');
  searchClose = document.getElementById('searchClose');
console.log(searchClick);
console.log(searchParent);
console.log(searchClose);

form.addEventListener('click', function() {
  console.log("clickme")
  searchParent.style.display = "inline-block";
  searchParent.children[0].style.display ="inline-block";
  searchClose.style.display="inline-block";
  form.style.display="none";
});

searchClose.addEventListener('click', function() {
  searchParent.style.display = "none";
  searchParent.children[1].style.display ="block";
  form.style.display="inline-block";
  searchClose.style.display="none";
});